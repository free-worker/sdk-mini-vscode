# laravel在线代码编辑器(纯前端vscode)

项目地址: [类库地址](https://gitee.com/free-worker/sdk-mini-vscode "类库地址")

## 在线实例

[在线示例https://vscode.i0358.cn](https://vscode.i0358.cn)
![示例图](https://gitee.com/free-worker/web-vscode-editor/raw/master/sample.jpg)

## docker 运行
[示例工程sample地址](https://gitee.com/free-worker/web-vscode-editor.git)

```
docker run -it -p 9011:9000 ccr.ccs.tencentyun.com/afan-public/mini-vscode:v1.0.3

or

docker run -it -p 9011:9000 -e "VSCODE_EDITOR_ROOT=/home/editor" -v ${PWD}:/home/editor ccr.ccs.tencentyun.com/afan-public/mini-vscode:v1.0.3

${PWD} 为你的代码目录
```


## 解决问题
1. 服务器资源受限无法使用websocket部署在线vscode,theia等在线ide　只能走http协议编辑文件 
2. 内侧环境下　无需复杂环境配置 改bug　直接请求http:xxxx/api/fs/view

## 功能
* 浏览代码
* 修改代码

## 安装

Require the `w7corp-pre/sdk-mini-vscode` package in your `composer.json` and update your dependencies:

```sh
composer require w7corp-pre/sdk-mini-vscode
```

## CORS 跨域
必须配置跨域允许 we7coreteam.gitee.io域名的请求
config/cors.php　修改如下

```php
'paths' => ['api/fs/*'], //允许跨域
```

## 配置

The defaults are set in `config/w7-vscode.php`. 发布配置:
```sh
php artisan vendor:publish --tag="w7-vscode"
```

### 环境变量

| Option                   | Description                 | Default value |
|--------------------------|-----------------------------|---------------|
| VSCODE_EDITOR_ENABLE    |   是否开启代码编辑          　| false        |
| VSCODE_SDK_API_HTTPS     |   主项目是否https            | false   |
| VSCODE_EDITOR_ROOT     |   代码主目录            | storage_path('/public')   |     |   主项目是否https            | false   |
> 当前代码storage目录　默认是/tmp

## License

Released under the MIT License, see [LICENSE](LICENSE).

[ico-version]: https://img.shields.io/packagist/v/fruitcake/laravel-cors.svg?style=flat-square

