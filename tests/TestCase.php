<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\VSCode\Tests;

use Faker\Factory;
use Orchestra\Testbench\TestCase as BenchTestCase;
use W7\Sdk\VSCode\Providers\VSCodeServiceProvider;

abstract class TestCase extends BenchTestCase {
	protected function setUp(): void {
		parent::setUp();

		app()->singleton(\Faker\Generator::class, function ($app) {
			return Factory::create('zh_CN');
		});
	}

	protected function getEnvironmentSetUp($app) {
		$app->register(VSCodeServiceProvider::class);
	}
}
