<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\VSCode\Tests;

use W7\Sdk\VSCode\Traits\StorageTrait;

/**
 * fs create file
 */
class TestFileCreate extends TestCase {
	use StorageTrait;

	/**
	 * @var string
	 */
	private $route = '';
	/**
	 * @var string
	 */
	private $txtFrom = 'from.txt';

	protected function setUp(): void {
		parent::setUp(); // TODO: Change the autogenerated stub
		$this->route = route('vs.code.write.file');
	}

	/**
	 * write file
	 */
	public function testFileCreate() {
		$this->storageDeleteFile($this->txtFrom);

		$this->postJson($this->route, [
			'uri' => $this->txtFrom,
		])->assertOk();
		$this->assertTrue($this->storageIsExists($this->txtFrom));
	}
}
