<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\VSCode\Tests\Actions;

use W7\Sdk\VSCode\Actions\SearchAction;

class SearchActionTest extends \W7\Sdk\VSCode\Tests\TestCase {
	public function test_search() {
		$result = app(SearchAction::class)->search('pub');
		$this->assertCount(1, $result);
	}
}
