<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\VSCode\Tests;

use W7\Sdk\VSCode\Traits\StorageTrait;

/**
 * test directory copy
 */
class TestDirectoryCopy extends TestCase {
	use StorageTrait;

	/**
	 * @var string
	 */
	private $route = '';
	/**
	 * @var string
	 */
	private $dirFrom = 'from';
	/**
	 * @var string
	 */
	private $dirTo = 'to';

	protected function setUp(): void {
		parent::setUp(); // TODO: Change the autogenerated stub
		$this->route = route('vs.code.copy');
	}

	/**
	 * test directory copy
	 */
	public function testDirectoryCopy() {
		$this->storageDeleteDir($this->dirFrom);
		$this->storageDeleteDir($this->dirTo);

		$this->storageMakeDir($this->dirFrom);
		$this->patchJson($this->route, ['uri' => $this->dirFrom, 'newuri' => $this->dirTo])->assertOk();
		$this->assertTrue($this->storageIsExists($this->dirTo));
	}
}
