<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\VSCode\Tests;

use W7\Sdk\VSCode\Traits\StorageTrait;

class TestSearchFile extends TestCase {
	use StorageTrait;

	/**
	 * @var string
	 */
	private $content = 'hello123456';
	/**
	 * @var string
	 */
	private $txtFile = '/api/test.txt';
	/**
	 * @var string
	 */
	private $route = 'vs.code.search.file';

	/**
	 * test stat file
	 */
	public function testFileSearch() {
		config(['app.debug' => true]);
		$this->route = route('vs.code.search.file', ['query' => 'test']);

		$this->storageDeleteFile($this->txtFile);

		$this->storagePutContent($this->txtFile, $this->content);
		$this->assertTrue($this->storageIsExists($this->txtFile));

		$response = $this->getJson($this->route);

		$responseJson = $response->json();

		//        $this->assertArrayHasKey('size', $responseJson);
		//        $this->assertArrayHasKey('type', $responseJson);
		//        $this->assertArrayHasKey('mtime', $responseJson);
		//        $this->assertArrayHasKey('ctime', $responseJson);
		//        $this->assertArrayHasKey('isSymbolicLink', $responseJson);

		ll($responseJson);
	}
}
