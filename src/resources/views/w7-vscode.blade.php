<?php ?>
        <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>web-editor</title>
    <style>
        *{
            margin: 0;
            padding: 0;
        }

        html,body{
            width: 100%;
            height: 100%;
        }

        .container{
            width: 100%;
            height: 100%;
            font-size: 0;

        }
        .web-editor {
            display: block;
            width: 100%;
            height: 100%;
            vertical-align:top;
            border-width: 0px;
        }
    </style>

</head>

<body aria-label="">
<div class = "container"  >
    <iframe src="{!!$vsUrl!!}" width="100%" height="100%" allowfullscreen class="web-editor" scrolling="no"></iframe>
{{--    <iframe src="https://we7coreteam.gitee.io/vscode-httpfilesystem-extension/test.html" width="100%" height="100%" allowfullscreen class="web-editor" scrolling="no"></iframe>--}}
</div>

</body>

</html>
