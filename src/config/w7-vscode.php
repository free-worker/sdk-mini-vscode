<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

return [
	'route' => [
		'namespace' => '\W7\Sdk\VSCode\Controller',
		'middleware' => ['w7.vscode'],
		'prefix' => '/api/fs/',
	],
	'enable' => env('VSCODE_EDITOR_ENABLE', false),
	'driver' => env('VSCODE_EDITOR_DRIVER', 'vscode'),
//	'token' => env('VSCODE_ENDITOR_TOKEN', 'token'),
	'vscode_git_page' => env('VSCODE_EDITOR_GITPAGE', '//we7coreteam.gitee.io/vscode-httpfilesystem-extension/test.html'),
	'mode' => env('VSCODE_EDITOR_MODE', 'iframe'), //redirect or iframe
	'enable_api_https' => env('VSCODE_SDK_API_HTTPS', false),
	'filesystem' => [
		'driver' => 'local',
		'root' => env('VSCODE_EDITOR_ROOT', storage_path('/public')),
	]
];
