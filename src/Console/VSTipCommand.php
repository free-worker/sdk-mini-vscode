<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\VSCode\Console;

use Illuminate\Console\Command;

/**
 *
 */
class VSTipCommand extends Command {
	protected $signature = 'vs:tip {--https=1} {--root=/home/s.we7.cc}';

	protected $description = 'php artisan vs:print_env';

	public function handle() {
		$this->line('VSCODE_EDITOR_ENABLE=true');
		$this->line('VSCODE_SDK_API_HTTPS='.($this->option('https') ? 'true' : 'false'));
		$this->line('VSCODE_EDITOR_ROOT='.$this->option('root'));
	}
}
