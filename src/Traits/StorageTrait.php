<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\VSCode\Traits;

use Illuminate\Support\Facades\Storage;

/**
 *
 * Trait StorageTrait
 */
trait StorageTrait {
	/**
	 * @param string $uri
	 * @return string
	 */
	protected function storagePath(string $uri = ''): string {
		if (empty($uri)) {
			return '';
		}
		return Storage::path($uri);
	}

	/**
	 * @param string $from
	 * @param string $to
	 */
	protected function storageMove(string $from = '', string $to = '') {
		if (empty($from) || empty($to)) {
			return;
		}
		Storage::move($from, $to);
	}

	/**
	 * @param string $path
	 * @param string $content
	 */
	protected function storagePutContent(string $path = '', string $content = '') {
		if (empty($path) || empty($content)) {
			return;
		}
		Storage::put($path, $content);
	}

	/**
	 * @param string $path
	 * @return string
	 */
	protected function storageGetContent(string $path = ''): string {
		if (empty($path)) {
			return '';
		}
		return Storage::get($path);
	}

	/**
	 * @param string $dir
	 */
	protected function storageMakeDir(string $dir = '') {
		if (empty($dir)) {
			return;
		}
		Storage::makeDirectory($dir);
	}

	/**
	 * @param string $file
	 */
	protected function storageDeleteFile(string $file = '') {
		if (empty($file)) {
			return;
		}
		if (!$this->storageIsExists($file)) {
			return;
		}
		Storage::delete($file);
	}

	/**
	 * @param string $dir
	 */
	protected function storageDeleteDir(string $dir = '') {
		if (empty($dir)) {
			return;
		}
		if (!$this->storageIsExists($dir)) {
			return;
		}
		Storage::deleteDirectory($dir);
	}

	/**
	 * @param string $str
	 * @return bool
	 */
	protected function storageIsExists(string $str = ''): bool {
		if (empty($str)) {
			return false;
		}
		return Storage::exists($str);
	}
}
