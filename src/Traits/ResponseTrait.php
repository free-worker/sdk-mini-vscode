<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\VSCode\Traits;

use Symfony\Component\HttpFoundation\Response as Code;

trait ResponseTrait {
	/**
	 * @param $status
	 * @param string $error
	 * @return \Illuminate\Http\JsonResponse
	 */
	protected function response($status, $error = '') {
		return response()->json(['error' => $error], $status, [], JSON_UNESCAPED_UNICODE);
	}

	protected function notFound() {
		return $this->response(Code::HTTP_NOT_FOUND);
	}

	/**
	 *  正常返回数据
	 * @param $data
	 * @param array $header
	 * @param null $cookie
	 * @return \Illuminate\Http\JsonResponse
	 */
	protected function ok($data = [], $header = [], $cookie = null) {
		$response = response()->json($data, 200, $header, JSON_UNESCAPED_UNICODE);
		if ($cookie) {
			$response->withCookie($cookie);
		}
		return $response;
	}

	/**
	 * 创建成功  201
	 * @param array $data
	 * @return \Illuminate\Http\Response
	 */
	protected function created(array $data) {
		return response($data, Code::HTTP_CREATED);
	}

	/**
	 *  更新成功 和 删除 是一个状态码 204
	 * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
	 */
	protected function updated() {
		return response('', Code::HTTP_NO_CONTENT);
	}

	/**
	 *  删除成功 204
	 * @return \Illuminate\Http\JsonResponse
	 */
	protected function deleted() {
		return $this->response(Code::HTTP_NO_CONTENT);
	}

	/**
	 *  出错了  400
	 * @param $error
	 * @return \Illuminate\Http\JsonResponse
	 */
	protected function error($error) {
		return $this->response(Code::HTTP_BAD_REQUEST, $error);
	}

	/**
	 * @param $error
	 * @param $code
	 * @return \Illuminate\Http\JsonResponse
	 */
	protected function successCode($error, $code) {
		return response()->json(['error' => $error, 'code' => $code], 200);
	}

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	protected function successApi() {
		return response()->json(['data' => 'success'], 200);
	}

	/**
	 *  无权限
	 * @param $error
	 * @return \Illuminate\Http\JsonResponse
	 */
	protected function noauth($error) {
		return $this->response(Code::HTTP_FORBIDDEN, $error);
	}

	/**
	 *  无权限
	 * @param $error
	 * @return \Illuminate\Http\JsonResponse
	 */
	protected function tooManyRequest($error) {
		return $this->response(Code::HTTP_TOO_MANY_REQUESTS, $error);
	}
}
