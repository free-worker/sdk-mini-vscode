<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\VSCode\Traits;

use Illuminate\Support\Facades\File;

/**
 * Trait FileTrait
 */
trait FileTrait {
	/**
	 * @param string $file
	 * @return bool
	 */
	protected function fileIsFile(string $file = ''): bool {
		if (empty($file)) {
			return false;
		}
		return File::isFile($file);
	}

	/**
	 * @param string $dir
	 * @return bool
	 */
	protected function fileIsDir(string $dir = ''): bool {
		if (empty($dir)) {
			return false;
		}
		return File::isDirectory($dir);
	}
}
