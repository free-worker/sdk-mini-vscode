<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\VSCode\Controller;

use Illuminate\Http\Request;
use W7\Sdk\VSCode\Domain\File;
use W7\Sdk\VSCode\Traits\ResponseTrait;

/**
 * delete file or directory
 */
class DeleteController {
	use ResponseTrait;

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function handle(Request $request) {
		$input = $request->all();
		if (!isset($input['uri'])) {
			throw new \InvalidArgumentException('Missing the required parameter $uri when calling delete');
		}
		$uri = $input['uri'];

		File::make($uri)->delete();

		return $this->ok([]);
	}
}
