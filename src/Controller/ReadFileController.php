<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\VSCode\Controller;

use Illuminate\Http\Request;
use W7\Sdk\VSCode\Domain\File;
use W7\Sdk\VSCode\Traits\ResponseTrait;

/**
 * read file
 */
class ReadFileController {
	use ResponseTrait;

	public function handle(Request $request) {
		$input = $request->all();
		if (!isset($input['uri'])) {
			throw new \InvalidArgumentException('Missing the required parameter $uri when calling readFile');
		}
		$uri = $input['uri'];

		$content = File::make($uri)->get();
		return $this->ok(['content' => $content]);
	}
}
