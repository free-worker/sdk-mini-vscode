<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\VSCode\Controller;

use Illuminate\Http\Request;
use W7\Sdk\VSCode\Actions\SearchAction;

class SearchFileController {
	public function handle(Request $request, SearchAction $searchAction) {
		$input = $request->all();
		if (!isset($input['query'])) {
			throw new \InvalidArgumentException('Missing the required parameter $query when calling stat');
		}
		$query = $input['query'];
		return $searchAction->search($query);
	}
}
