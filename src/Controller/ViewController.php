<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\VSCode\Controller;

use Illuminate\Http\Request;

class ViewController {
	public function html(Request $request) {
		$root = config('w7-vscode.vscode_git_page');
		$scheme = config('w7-vscode.enable_api_https') ? 'w7s' : 'w7';
		$path = '';
		$authority = $request->getHttpHost();
		$fullUrl = $root . '?folder='.$scheme.'://'.$authority.'/'.$path;
		if (config('w7-vscode.mode') === 'redirect') {
			return redirect('https:'.$fullUrl);
		}
		return view('w7-vscode::w7-vscode', ['vsUrl' => $fullUrl]);
	}

	public function test(Request $request) {
		return [
			'headers' => $request->header(),
			'server' => $request->server(),
			'ip' => $request->ip(),
			'authority' => $request->getHttpHost(),
		];
	}
}
