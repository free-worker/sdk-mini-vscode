<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\VSCode\Controller\Middleware;

use Illuminate\Http\Request;

class LaravelMiddleware {
	/**
	 *
	 * @param Request $request
	 * @param \Closure $next
	 */
	public function handle($request, \Closure $next) {
		if (!($request->bearerToken() === config('w7-vscode.token'))) {
//			return response('', 403);
		}
		if (config('w7-vscode.enable', false) !== true) {
			return response()->json(['error' => 'not enable web editor please config VSCODE_ENDITOR_ENABLE=true'], 403);
		}
		return $next($request);
	}
}
