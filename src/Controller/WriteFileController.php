<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\VSCode\Controller;

use Illuminate\Http\Request;
use W7\Sdk\VSCode\Domain\File;
use W7\Sdk\VSCode\Traits\ResponseTrait;

/**
 * write file
 */
class WriteFileController {
	use ResponseTrait;

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Illuminate\Contracts\Filesystem\FileExistsException
	 */
	public function handle(Request $request) {
		$input = $request->all();
		if (!isset($input['uri'])) {
			throw new \InvalidArgumentException('Missing the required parameter $uri when calling writeFile');
		}
		$uri = $input['uri'];

		//        if (!isset($input['content'])) {
		//            throw new \InvalidArgumentException('Missing the required parameter $content when calling writeFile');
		//        }
		$content = data_get($input, 'content', null);
		if (is_null($content)) {
			$content = '';
		}
		File::make($uri)->write($content);

		return $this->ok([]);
	}
}
