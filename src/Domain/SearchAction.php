<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\VSCode\Actions;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 *
 */
class SearchAction {
	public function search(string $query) : array {
		//        $query = preg_replace('/[\-\\\{\}\+\?\|\^\$\.\,\[\]\(\)\#\s]\/g/', '\\$&', $query);
		//        $query = preg_replace('/[\*]\/g/', '.*', $query);
		$files = Storage::allFiles();
		return array_filter($files, function (string $file) use ($query) {
			$filename = pathinfo($file, PATHINFO_FILENAME);
			if (Str::contains($filename, $query)) {
				return true;
			}
			return false;
		});
	}
}
