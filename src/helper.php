<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

if (!function_exists('ll')) {
	function ll(...$vars) {
		$option = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE;
		$traces = debug_backtrace();

		$args = [
			"{$traces[0]['file']}:{$traces[0]['line']}",
		];
		foreach ($vars as $var) {
			if ($var instanceof Model ||
				$var instanceof Collection) {
				$args[] = $var->toJson($option);
			} else {
				$args[] = json_encode($var, $option);
			}
		}

		foreach ($args as $x) {
			dump($x);
		}
	}
}

if (!function_exists('ul')) {
	function ul(...$vars) {
		$option = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE;
		$traces = debug_backtrace();

		$args = [
			"{$traces[0]['file']}:{$traces[0]['line']}",
		];
		foreach ($vars as $var) {
			if ($var instanceof Model
				|| $var instanceof Collection
				|| $var instanceof Arrayable) {
				$args[] = $var->toJson($option);
			} else {
				$args[] = json_encode($var, $option);
			}
		}

		foreach ($args as $x) {
			dump($x);
		}
	}
}
