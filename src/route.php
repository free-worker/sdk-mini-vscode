<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

use Illuminate\Support\Facades\Route;

Route::group([
	'prefix' => config('w7-vscode.route.prefix'),
	'middleware' => config('w7-vscode.route.middleware'),
	'namespace' => config('w7-vscode.route.namespace')
], function (\Illuminate\Routing\Router $router) {
	/**
	 * patch copy
	 * Summary: copy
	 * Notes: copy
	 */
	$router->patch('/copy', 'CopyController@handle')
		->name('vs.code.copy');
	/**
	 * post createDirectory
	 * Summary: createDirectory
	 * Notes: createDirectory
	 */
	$router->post('/createDirectory', 'CreateDirectoryController@handle')
		->name('vs.code.create.directory');
	/**
	 * delete delete
	 * Summary: delete
	 * Notes: delete
	 */
	$router->delete('/delete', 'DeleteController@handle')
		->name('vs.code.delete');
	/**
	 * get readDirectory
	 * Summary: readDirectory
	 * Notes: readDirectory
	 */
	$router->get('/readDirectory', 'ReadDirectoryController@handle')
		->name('vs.code.read.directory');
	/**
	 * get readFile
	 * Summary: Find pet by ID
	 * Notes: Returns a single pet
	 */
	$router->get('/readFile', 'ReadFileController@handle')
		->name('vs.code.read.file');
	/**
	 * put rename
	 * Summary: Place an order for a pet
	 * Notes:
	 */
	$router->put('/rename', 'RenameController@handle')
		->name('vs.code.rename');
	/**
	 * get stat
	 * Summary: filesystem stat
	 * Notes:
	 */
	$router->get('/stat', 'StatController@handle')
		->name('vs.code.stat');
	/**
	 * post writeFile
	 * Summary: uploads an image
	 * Notes:
	 */
	$router->post('/writeFile', 'WriteFileController@handle')
		->name('vs.code.write.file');

	/**
	 *
	 */
	$router->get('/searchFile', 'SearchFileController@handle')
		->name('vs.code.search.file');

	$router->get('/view', 'ViewController@html')
		->name('vs.code.html');

	$router->get('/test', 'ViewController@test')
		->name('vs.code.test');
});
