<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\VSCode\Exceptions;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Contracts\Support\Responsable;
use League\Flysystem\UnreadableFileException;

class VSCodeLowVersionExceptionHandler implements ExceptionHandler {
	/**
	 * @var ExceptionHandler|null
	 */
	private $previous;

	public function __construct(ExceptionHandler $previous = null) {
		$this->previous = $previous;
	}

	public function report(\Exception $exception) {
		$this->previous === null ?: $this->previous->report($exception);
	}

	public function render($request, \Exception $e) {
		if ($e instanceof Responsable) {
			return $e->toResponse($request);
		}
		if ($e instanceof FileNotFoundException
			|| $e instanceof \League\Flysystem\FileNotFoundException
			|| $e instanceof FileNotFoundException) {
			return response()->json([], 404);
		}
		if ($e instanceof UnreadableFileException) {
			return response()->json([], 403);
		}

		$response = $this->previous === null ? null : $this->previous->render($request, $e);
		return $response;
	}

	public function shouldReport(\Exception $e) {
		return $this->previous ? $this->previous->shouldReport($e) : false;
	}

	public function renderForConsole($output, \Exception $e) {
		$this->previous === null ?: $this->previous->renderForConsole($output, $e);
	}
}
