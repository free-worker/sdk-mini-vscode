<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\VSCode\Exceptions;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Contracts\Support\Responsable;
use League\Flysystem\UnreadableFileException;
use Throwable;

class VSCodeExceptionHandler implements ExceptionHandler {
	/**
	 * @var ExceptionHandler|null
	 */
	private $previous;

	public function __construct(ExceptionHandler $previous = null) {
		$this->previous = $previous;
	}

	public function report(\Throwable $exception) {
		$this->previous === null ?: $this->previous->report($exception);
	}

	public function render($request, \Throwable $e) {
		if ($e instanceof Responsable) {
			return $e->toResponse($request);
		}

		if ($e instanceof FileNotFoundException
			|| $e instanceof \League\Flysystem\FileNotFoundException
			|| $e instanceof FileNotFoundException) {
			return response()->json([], 404);
		}
		if ($e instanceof UnreadableFileException) {
			return response()->json([], 403);
		}

		$response = $this->previous === null ? null : $this->previous->render($request, $e);
		return $response;
	}

	/**
	 * {@inheritdoc}
	 */
	public function renderForConsole($output, \Throwable $exception) {
		/* @var OutputInterface $output */
		$this->previous === null ?: $this->previous->renderForConsole($output, $exception);
	}

	public function shouldReport(Throwable $e) {
		return $this->previous ? $this->previous->shouldReport($e) : false;
	}
}
