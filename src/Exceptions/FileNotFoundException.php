<?php

/**
 * WeEngine Api System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\VSCode\Exceptions;

use Illuminate\Contracts\Support\Responsable;

class FileNotFoundException extends \Illuminate\Contracts\Filesystem\FileNotFoundException implements Responsable {
	public function toResponse($request) {
		return response()->json([], 404);
	}
}
